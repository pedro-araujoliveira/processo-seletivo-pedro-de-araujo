/* const form = document.getElementById('cadastro');
const NOME = document.getElementById('NOME');
const FK_SETOR = document.getElementById('FK_SETOR');
const NU_SALARIO = document.getElementById('NU_SALARIO');
const DS_EMAIL = document.getElementById('DS_EMAIL');
const NU_IDADE = document.getElementById('NU_IDADE');


form.addEventListener("submit", (e) => {
	e.preventDefault();
	console.log('teste')
})
*/




//*		  <label for="NOME">Nome:</label><br>
//*		  <input type="text" id="NOME" name="nome"><br><br>
//*		  <label for="FK_SETOR">Setor:</label><br>
//*		  <input type="text" id="FK_SETOR" name="setor"><br><br>
//*		  <label for="NU_SALARIO">Salario:</label><br>
//*		  <input type="text" id="NU_SALARIO" name="salario"><br><br>
//*		  <label for="DS_EMAIL">Email:</label><br>
//*		  <input type="text" id="DS_EMAIL" name="email"><br><br>
//*		  <label for="NU_IDADE">Idade:</label><br>
//*		  <input type="text" id="NU_IDADE" name="idade"><br><br>

var cadastro = new Vue({
	el:"#cadastro",
    data: {
        userData: {}
    },
    created: function() {
		this.getUserFormUserData();
    },
    methods:{
		getUserFormUserData() {
			const form = document.getElementById('cadastro');
			const NOME = document.getElementById('NOME');
			const FK_SETOR = document.getElementById('FK_SETOR');
			const NU_SALARIO = document.getElementById('NU_SALARIO');
			const DS_EMAIL = document.getElementById('DS_EMAIL');
			const NU_IDADE = document.getElementById('NU_IDADE');
			this.userData = {
				NOME,
				FK_SETOR,
				NU_SALARIO,
				DS_EMAIL,
				NU_IDADE
			}
		},
		createUser() {
			const body = this.userData;
			axios.post("/funcionarios/rest/funcionarios", body)
				.then(response => {
					console.log('res', response.data)
			}).catch(function (error) {
				console.error(error, 'erro no post')
			}).finally(function() {
			});
		}
    }
});